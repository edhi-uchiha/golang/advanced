package infra

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
)

var (
	cfg *viper.Viper
)

type Infra interface {
	ApiServer() string
	Config() *viper.Viper
	SqlDb() *sql.DB
}

type infra struct{}

func NewInfra() Infra {
	return &infra{}
}

func (i *infra) SqlDb() *sql.DB {
	dbUser := i.Config().GetString("DB_USER")
	dbPassword := i.Config().GetString("DB_PASSWORD")
	dbHost := i.Config().GetString("DB_HOST")
	dbPort := i.Config().GetString("DB_PORT")
	dbSchema := i.Config().GetString("DB_SCHEMA")
	dbEngine := i.Config().GetString("DB_ENGINE")

	db, err := sql.Open(dbEngine, fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPassword, dbHost, dbPort, dbSchema))

	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}

	return db
}

func (i *infra) ApiServer() string {
	host := i.Config().GetString("HTTP_HOST")
	port := i.Config().GetString("HTTP_PORT")
	return fmt.Sprintf("%s:%s", host, port)
}

func (i *infra) Config() *viper.Viper {
	viper.AddConfigPath(".")
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()
	viper.ReadInConfig()
	cfg = viper.GetViper()
	return cfg
}
