package manager

import (
	"gitlab.com/edhi-uchiha/golang/advanced/infra"
	"gitlab.com/edhi-uchiha/golang/advanced/services"
)

type ServiceManager interface {
	CustomerService() services.CustomerService
}

type serviceManager struct {
	repoManager RepoManager
}

func (sm *serviceManager) CustomerService() services.CustomerService {
	return services.NewCustomerService(sm.repoManager.CustomerRepo())
}

func NewServiceManager(infra infra.Infra) ServiceManager {
	return &serviceManager{
		NewRepoManager(infra),
	}
}
