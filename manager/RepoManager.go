package manager

import (
	"database/sql"

	"gitlab.com/edhi-uchiha/golang/advanced/infra"
	"gitlab.com/edhi-uchiha/golang/advanced/repositories"
)

type RepoManager interface {
	CustomerRepo() repositories.CustomerRepo
}

type repoManager struct {
	sqlConn *sql.DB
	infra   infra.Infra
}

func (rm *repoManager) CustomerRepo() repositories.CustomerRepo {
	return repositories.NewCustomerRepoDBImpl(rm.sqlConn)
}

func NewRepoManager(infra infra.Infra) RepoManager {
	return &repoManager{infra.SqlDb(), infra}
}
