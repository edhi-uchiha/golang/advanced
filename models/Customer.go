package models

// Customer struct with firstName, lastName and address properties
type Customer struct {
	Id        string `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Address   string `json:"address"`
}

func (c *Customer) SetId(id string) {
	c.Id = id
}

func (c *Customer) GetId() string {
	return c.Id
}

func (c *Customer) GetFirstname() string {
	return c.FirstName
}

func (c *Customer) SetFirstname(firstName string) {
	c.FirstName = firstName
}

func (c *Customer) GetLastName() string {
	return c.LastName
}

func (c *Customer) SetLastName(lastName string) {
	c.LastName = lastName
}

func (c *Customer) GetAddress() string {
	return c.Address
}

func (c *Customer) SetAddress(address string) {
	c.Address = address
}

// Customer toString func return string format of Customer : function receiver
func (c *Customer) ToString() string {
	return c.Id + " " + c.FirstName + " " + c.LastName + " " + c.Address
}

// NewCustomerDefault is func to declare new Customer
func NewCustomerDefault() *Customer {
	return &Customer{}
}

// NewCustomer is func to declare new Customer with default value
func NewCustomer(firstName, lastName, address string) *Customer {
	return &Customer{
		FirstName: firstName,
		LastName:  lastName,
		Address:   address,
	}
}

// ExistingCustomer is func to declare new Customer with default value
func ExistingCustomer(id, firstName, lastName, address string) *Customer {
	return &Customer{
		Id:        id,
		FirstName: firstName,
		LastName:  lastName,
		Address:   address,
	}
}
