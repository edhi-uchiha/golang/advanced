package services

import (
	"gitlab.com/edhi-uchiha/golang/advanced/models"
	"gitlab.com/edhi-uchiha/golang/advanced/repositories"
)

type CustomerService interface {
	Register(customer *models.Customer) (*models.Customer, error)
	UserInfo(id string) (*models.Customer, error)
	CustomerList(numberPage, totalPerPage int) ([]*models.Customer, error)
	Unregister(id string) error
	Edit(customer *models.Customer) (*models.Customer, error)
}

type CustomerServiceImpl struct {
	customerRepo repositories.CustomerRepo
}

func (c *CustomerServiceImpl) Register(customer *models.Customer) (*models.Customer, error) {

	if customer.GetFirstname() == "" || customer.GetLastName() == "" || customer.GetAddress() == "" {
		panic("Firstname, lastname, or address is required")
	}

	newCustomer, err := c.customerRepo.Save(customer)

	if err != nil {
		return nil, err
	}

	return newCustomer, nil

}

func (c *CustomerServiceImpl) UserInfo(id string) (*models.Customer, error) {

	customer, err := c.customerRepo.FindById(id)

	if err != nil {
		return nil, err
	}

	return customer, nil
}

func (c *CustomerServiceImpl) CustomerList(numberPage, totalPerPage int) ([]*models.Customer, error) {

	// ini hanya akal-akalan
	// searching seharusnya menggunakan limit di query sql

	var start = (numberPage - 1) * totalPerPage

	if start < 0 {
		start = 0
	}

	return c.customerRepo.FindAll(start, totalPerPage)
}

func (c *CustomerServiceImpl) Unregister(id string) error {

	customer, err := c.customerRepo.FindById(id)

	if err != nil {
		return err
	}

	if customer == nil {
		return err
	} else {
		c.customerRepo.Delete(id)
	}

	return nil
}

func (c *CustomerServiceImpl) Edit(customer *models.Customer) (*models.Customer, error) {

	customerToEdit, err := c.UserInfo(customer.GetId())

	if err != nil {
		return nil, err
	}

	if customerToEdit == nil {
		panic("Customer not found")
	}

	if customer.GetFirstname() != "" {
		customerToEdit.SetFirstname(customer.GetFirstname())
	}
	if customer.GetLastName() != "" {
		customerToEdit.SetLastName(customer.GetLastName())
	}

	if customer.GetAddress() != "" {
		customerToEdit.SetAddress(customer.GetAddress())
	}

	updatedCustomer, errorGet := c.customerRepo.Update(customerToEdit)

	if errorGet != nil {
		return nil, errorGet
	}

	return updatedCustomer, nil
}

func NewCustomerService(custRepo repositories.CustomerRepo) CustomerService {
	return &CustomerServiceImpl{
		customerRepo: custRepo,
	}
}
