package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/edhi-uchiha/golang/advanced/controllers"
	"gitlab.com/edhi-uchiha/golang/advanced/infra"
)

type App struct {
	router *gin.Engine
	infra  infra.Infra
}

func (app *App) run() {

	controllers.NewAppRouter(app.infra, app.router).InitRouter()

	if err := app.router.Run(app.infra.ApiServer()); err != nil {
		panic(err)
	}

}

func NewApp() *App {

	router := gin.Default()
	appInfra := infra.NewInfra()
	return &App{
		router,
		appInfra,
	}
}

func main() {

	NewApp().run()

}
