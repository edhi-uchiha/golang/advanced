package controllers

import (
	"github.com/gin-gonic/gin"
	httResponse "gitlab.com/edhi-uchiha/golang/advanced/http-utils/http-response"
	"gitlab.com/edhi-uchiha/golang/advanced/infra"
	"gitlab.com/edhi-uchiha/golang/advanced/manager"
)

type appRouter struct {
	router *gin.Engine
	infra  infra.Infra
}

func NewAppRouter(infra infra.Infra, router *gin.Engine) *appRouter {
	return &appRouter{router, infra}
}

func (ar *appRouter) InitRouter() {
	ar.router.GET("/ping", helloController)

	serviceManager := manager.NewServiceManager(ar.infra)
	NewCustomerDelivery(ar.router, serviceManager.CustomerService(), httResponse.NewJsonResponder()).InitRoute()

}
