package controllers

import (
	"net/http"
	"strconv"

	"gitlab.com/edhi-uchiha/golang/advanced/models"

	"github.com/gin-gonic/gin"

	httResponse "gitlab.com/edhi-uchiha/golang/advanced/http-utils/http-response"
	"gitlab.com/edhi-uchiha/golang/advanced/services"
)

type CustomerController struct {
	customerService services.CustomerService
	responder       httResponse.IResponder
}

func NewCustomerController(service services.CustomerService, responder httResponse.IResponder) *CustomerController {
	return &CustomerController{
		customerService: service,
		responder:       responder,
	}
}

func (cc *CustomerController) getCustomerPaginController(c *gin.Context) {

	// localhost:8080/customers?pageNo=1&totalperPage=10

	pageNo := c.DefaultQuery("pageNo", "1")
	totalPerPage := c.DefaultQuery("totalPerPage", "10")

	iPageNo, _ := strconv.Atoi(pageNo)
	iTotalPerPage, _ := strconv.Atoi(totalPerPage)

	customers, err := cc.customerService.CustomerList(iPageNo, iTotalPerPage)

	if err != nil {
		cc.responder.SetContext(c).ErrorResponder(http.StatusInternalServerError, "500", "Internal Server Error.")
	}

	cc.responder.SetContext(c).PagingResponder(http.StatusOK, customers, iPageNo, len(customers))
}

func (cc *CustomerController) getCustomerById(c *gin.Context) {

	id := c.Param("id")

	customer, err := cc.customerService.UserInfo(id)

	if err != nil {
		cc.responder.SetContext(c).ErrorResponder(http.StatusBadRequest, "400", "Bad Request.")
	}

	cc.responder.SetContext(c).SingleResponder(http.StatusOK, customer)
}

func (cc *CustomerController) newCustomer(c *gin.Context) {

	var customer models.Customer
	if err := c.ShouldBindJSON(&customer); err != nil {
		cc.responder.SetContext(c).ErrorResponder(http.StatusBadRequest, "400", "Bad Request.")
	}
	createdCustomer, err := cc.customerService.Register(&customer)
	if err != nil {
		cc.responder.SetContext(c).ErrorResponder(http.StatusBadRequest, "400", "Bad Request.")
	}

	cc.responder.SetContext(c).SingleResponder(http.StatusCreated, createdCustomer)

}

func (cc *CustomerController) updateCustomer(c *gin.Context) {

	var customer models.Customer
	if err := c.ShouldBindJSON(&customer); err != nil {
		cc.responder.SetContext(c).ErrorResponder(http.StatusBadRequest, "400", "Bad Request.")
	}
	updatedCustomer, err := cc.customerService.Edit(&customer)
	if err != nil {
		cc.responder.SetContext(c).ErrorResponder(http.StatusBadRequest, "400", "Bad Request.")
	}
	cc.responder.SetContext(c).SingleResponder(http.StatusOK, updatedCustomer)
}

func (cc *CustomerController) deleteCustomer(c *gin.Context) {

	id := c.Param("id")

	err := cc.customerService.Unregister(id)
	if err != nil {
		cc.responder.SetContext(c).ErrorResponder(http.StatusNotFound, "404", "Customer not found.")
	}

	cc.responder.NoContentResponder(http.StatusNoContent)
}
