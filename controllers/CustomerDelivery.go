package controllers

import (
	"github.com/gin-gonic/gin"
	httResponse "gitlab.com/edhi-uchiha/golang/advanced/http-utils/http-response"
	"gitlab.com/edhi-uchiha/golang/advanced/services"
)

type CustomerDelivery struct {
	router     *gin.Engine
	controller *CustomerController
}

func NewCustomerDelivery(router *gin.Engine, customerService services.CustomerService, responder httResponse.IResponder) AppDelivery {
	return &CustomerDelivery{
		router,
		NewCustomerController(customerService, responder),
	}
}

func (cd *CustomerDelivery) InitRoute() {
	customerRouter := cd.router.Group("/customers")
	{
		customerRouter.GET("", cd.controller.getCustomerPaginController)
		customerRouter.GET("/:id", cd.controller.getCustomerById)
		customerRouter.POST("", cd.controller.newCustomer)
		customerRouter.PUT("", cd.controller.updateCustomer)
		customerRouter.DELETE("/:id", cd.controller.deleteCustomer)
	}
}
