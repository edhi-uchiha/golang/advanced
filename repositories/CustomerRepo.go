package repositories

import (
	"gitlab.com/edhi-uchiha/golang/advanced/models"
)

type CustomerRepo interface {
	Save(customer *models.Customer) (*models.Customer, error)
	Update(customer *models.Customer) (*models.Customer, error)
	Delete(id string) error
	FindAll(numberPage, totalPerPage int) ([]*models.Customer, error)
	FindById(id string) (*models.Customer, error)
	FindByFirstName(firstName string) []*models.Customer
}
