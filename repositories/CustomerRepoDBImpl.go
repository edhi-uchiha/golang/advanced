package repositories

import (
	"database/sql"

	guuid "github.com/google/uuid"
	"gitlab.com/edhi-uchiha/golang/advanced/models"
)

type CustomerRepoDBImpl struct {
	db *sql.DB
}

func (repo *CustomerRepoDBImpl) Save(cust *models.Customer) (*models.Customer, error) {

	id := guuid.New()
	cust.SetId(id.String())

	sql := "INSERT INTO customers(id, first_name, last_name, address) VALUE (?, ?, ?, ?)"
	stmt, stmtErr := repo.db.Prepare(sql)
	if stmtErr != nil {
		return nil, stmtErr
	}

	_, err := stmt.Exec(cust.GetId(), cust.GetFirstname(), cust.GetLastName(), cust.GetAddress())

	if err != nil {
		return nil, err
	}

	customer, errorGet := repo.FindById(cust.GetId())

	if errorGet != nil {
		return nil, err
	}

	return customer, nil
}

func (repo *CustomerRepoDBImpl) Update(customer *models.Customer) (*models.Customer, error) {

	sql := "UPDATE customers SET first_name = ?, last_name = ?, address = ? WHERE id = ?"
	stmt, stmtErr := repo.db.Prepare(sql)
	if stmtErr != nil {
		return nil, stmtErr
	}

	_, err := stmt.Exec(customer.GetFirstname(), customer.GetLastName(), customer.GetAddress(), customer.GetId())
	if err != nil {
		return nil, err
	}

	customer, errorGet := repo.FindById(customer.GetId())

	if errorGet != nil {
		return nil, err
	}

	return customer, nil
}

func (repo *CustomerRepoDBImpl) Delete(id string) error {

	sql := "DELETE FROM customers WHERE id=?"

	stmt, stmtErr := repo.db.Prepare(sql)
	if stmtErr != nil {
		return stmtErr
	}

	_, err := stmt.Exec(id)

	if err != nil {
		return err
	}

	return nil
}

func (repo *CustomerRepoDBImpl) FindAll(numberPage, totalPerPage int) ([]*models.Customer, error) {

	sql := "SELECT * FROM customers LIMIT ? , ?"
	stmt, err := repo.db.Prepare(sql)

	if err != nil {
		return nil, err
	}

	rows, err := stmt.Query(numberPage, totalPerPage)
	if err != nil {
		panic(err)
	}

	var customers []*models.Customer
	for rows.Next() {
		customer := new(models.Customer)
		err := rows.Scan(&customer.Id, &customer.FirstName, &customer.LastName, &customer.Address)
		if err != nil {
			panic(err)
		}
		customers = append(customers, customer)
	}

	return customers, nil
}

func (repo *CustomerRepoDBImpl) FindById(id string) (*models.Customer, error) {

	sql := "SELECT * FROM customers WHERE id=?"
	stmt, err := repo.db.Prepare(sql)

	if err != nil {
		return nil, err
	}
	row := stmt.QueryRow(id)

	customer := new(models.Customer)
	if err := row.Scan(&customer.Id, &customer.FirstName, &customer.LastName, &customer.Address); err != nil {
		return nil, err
	}

	return customer, nil

}

func (repo *CustomerRepoDBImpl) FindByFirstName(firstName string) []*models.Customer {

	customers := make([]*models.Customer, 0)
	return customers

}

func NewCustomerRepoDBImpl(dbConn *sql.DB) CustomerRepo {
	return &CustomerRepoDBImpl{
		db: dbConn,
	}
}
