package repositories

import (
	guuid "github.com/google/uuid"
	"gitlab.com/edhi-uchiha/golang/advanced/models"
)

type CustomerRepoImpl struct {
	customers []*models.Customer
}

func (repo *CustomerRepoImpl) Save(customer *models.Customer) (*models.Customer, error) {

	id := guuid.New()
	customer.SetId(id.String())

	repo.customers = append(repo.customers, customer)
	return customer, nil
}

func (repo *CustomerRepoImpl) Update(customer *models.Customer) (*models.Customer, error) {

	var updatedCustomer *models.Customer
	for _, cust := range repo.customers {
		if customer.GetId() == cust.GetId() {
			updatedCustomer = cust
			break
		}
	}

	updatedCustomer.SetFirstname(customer.GetFirstname())
	updatedCustomer.SetLastName(customer.GetLastName())
	updatedCustomer.SetAddress(customer.GetAddress())

	return updatedCustomer, nil
}

func (repo *CustomerRepoImpl) Delete(id string) error {

	newList := make([]*models.Customer, 0)

	for _, cust := range repo.customers {
		if id == cust.GetId() {
			continue
		}
		newList = append(newList, cust)
	}

	repo.customers = newList

	return nil
}

func (repo *CustomerRepoImpl) FindAll(numberPage, totalPerPage int) ([]*models.Customer, error) {

	return repo.customers, nil
}

func (repo *CustomerRepoImpl) FindById(id string) (*models.Customer, error) {

	customer := models.NewCustomerDefault()
	for _, cust := range repo.customers {
		if id == cust.GetId() {
			customer = cust
		}
	}

	return customer, nil

}

func (repo *CustomerRepoImpl) FindByFirstName(firstName string) []*models.Customer {

	customers := make([]*models.Customer, 0)
	for _, cust := range repo.customers {
		if firstName == cust.GetFirstname() {

			customers = append(customers, cust)

		}
	}

	return customers

}

func NewCustomerRepo() CustomerRepo {

	repo := make([]*models.Customer, 0)

	repo = append(repo, models.ExistingCustomer("123", "Edi", "Uchiha", "Lampung"))
	repo = append(repo, models.ExistingCustomer("124", "Narto", "Uzumaki", "Konoha"))
	repo = append(repo, models.ExistingCustomer("125", "Saskeh", "Uchiha", "Konoha"))
	repo = append(repo, models.ExistingCustomer("126", "Neji", "Hyuga", "Konoha"))

	return &CustomerRepoImpl{

		customers: repo,
	}
}
